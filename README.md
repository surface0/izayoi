# Seizu Development Forge

_Seizu Development Forge_ は個人研究、フリーウェア開発を目的としたプロジェクトの総称です。  
ソースコードの2次利用などは添付のライセンス表記に従ってください。

## BitmapLibrary(.NET Core2.0)

研究用画像処理ライブラリ。
`Seizu.Image`ネームスペース以下で参照できます。

### TODO

- リサイズフィルタ
    - ~~NearestNeighbor~~
    - ~~Bilinear~~
    - ~~Mitchel~~
    - ~~Bicubic~~
    - ~~CatmullRom~~
    - ~~Lanczos~~
    - ~~BSpline~~
    - ~~AreaAverage~~
- ローテーションフィルタ
- 減色
    - ポスタリゼーション
    - グレースケール
- YUV変換
    - クロマサブサンプリング

## Izayoi(.NET Core2.0)

`BitmapLibrary` を利用したCLIアプリケーション。