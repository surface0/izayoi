﻿using System.Drawing;
using Seizu.Image.Resize;
using Seizu.Image.Interporation;

namespace Seizu.Image
{
    public class BitmapUtil
    {
        public static Bitmap Resize(Bitmap bmp, int width, int height, ResizeInterporation interporation = ResizeInterporation.Bicubic, InterporationOption option = null)
        {
            if (option == null)
            {
                option = InterporationOption.Default;
            }

            var ret = ResizeFilter.Apply(bmp, width, height, interporation, option);

            /*
            if (option.GammaCollect)
            {
                ret = ColorUtil.Gamma.Collect(ret);
            }
            */
            return ret;
        }

        public static Bitmap Rotate(Bitmap bmp, double angle, ResizeInterporation interporation = ResizeInterporation.Bicubic)
        {
            return RotateFilter.Apply(bmp, angle, interporation);
        }
    }
}
