﻿using System;
using System.Collections.Generic;

namespace Seizu.Image.Interporation
{
    /// <summary>
    /// Spline16/36/64による重み付け
    /// http://www.geocities.jp/w_bean17/spline.html
    /// </summary>
    internal class Spline : DistanceInterporation
    {
        public override float RefRange { get; } = 3.0f;

        protected static readonly Dictionary<float, Func<float, float>> availableRefRanges = new Dictionary<float, Func<float, float>>()
        {
            { 2.0f, Spline.calcWeight16 },
            { 3.0f, Spline.calcWeight36 },
            { 4.0f, Spline.calcWeight64 },
        };

        public Spline(InterporationOption option) : base(option)
        {
            if (option.RefRange != InterporationOption.RefRangeAuto)
            {
                if (!availableRefRanges.ContainsKey(option.RefRange))
                {
                    throw new ArgumentException(@"Invalid RefRange");
                }

                RefRange = option.RefRange;
            }
        }

        #region 参照距離別重み付け関数
        /// <summary>
        /// RefRange = 2の時の重み付け
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        protected static float calcWeight16(float d)
        {
            if (d > 2.0f)
            {
                return .0f;
            }
            else if(d < 1.0f)
            {
                return (((5.0f * d - 9.0f) * d - 1.0f) * d + 5.0f) / 5.0f;
            }

            return (((-5.0f * d + 27.0f) * d - 46.0f) * d + 24.0f) / 15.0f;
        }

        /// <summary>
        /// RefRange = 3の時の重み付け
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        protected static float calcWeight36(float d)
        {
            if (d > 3.0f)
            {
                return .0f;
            }
            else if (d < 1.0f)
            {
                return (((247.0f * d - 453.0f) * d - 3.0f) * d + 209.0f) / 209.0f;
            }
            else if (d < 2.0f)
            {
                return (((-114.0f * d + 612.0f) * d - 1038.0f) * d + 540.0f) / 209.0f;
            }

            return (((19.0f * d - 159.0f) * d + 434.0f) * d - 384.0f) / 209.0f;
        }

        /// <summary>
        /// RefRange = 4の時の重み付け
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        protected static float calcWeight64(float d)
        {
            if (d > 4.0f)
            {
                return .0f;
            }
            else if (d < 1.0f)
            {
                return (((3479.0f * d - 6387.0f) * d - 3.0f) * d + 2911.0f) / 2911.0f;
            }
            else if (d < 2.0f)
            {
                return (((-1704.0f * d + 9144.0f) * d - 15504.0f) * d + 8064.0f) / 2911.0f;
            }
            else if (d < 3.0f)
            {
                return (((426.0f * d - 3564.0f) * d + 9726.0f) * d - 8604.0f) / 2911.0f;
            }

            return (((-71.0f * d + 807.0f) * d - 3022.0f) * d + 3720.0f) / 2911.0f;
        }
        #endregion

        public override float CalcWeight(float distance)
        {
            distance = Math.Abs(distance);

            switch ((int)RefRange)
            {
                case 2:
                    return calcWeight16(distance);

                case 4:
                    return calcWeight64(distance);

                case 3:
                default:
                    return calcWeight36(distance);
            }
        }
    }
}
