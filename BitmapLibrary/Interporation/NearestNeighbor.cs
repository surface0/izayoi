﻿using System;

namespace Seizu.Image.Interporation
{
    /// <summary>
    /// ニアレストネイバー法による重み付け。
    /// </summary>
    internal class NearestNeighbor : DistanceInterporation
    {
        public override float RefRange { get; } = .0f;

        public NearestNeighbor(InterporationOption option) : base (option) { }

        public override float CalcWeight(float distance)
        {
            return 1.0f;
        }
        /*
        public override OffsetCoefList[] MakeWeightListArray(int srcLength, int dstLength, int offsetMultiplier = 1)
        {
            double scale = (double)srcLength / dstLength;

            var ret = new OffsetCoefList[dstLength];

            for (int i = 0; i < dstLength; i++)
            {
                ret[i] = new OffsetCoefList(1) {
                    { Math.Min(srcLength - 1, (int)(i * scale + 0.5)) * offsetMultiplier, 1.0f }
                };
            }

            return ret;
        }
        */
    }
}
