﻿using System;

namespace Seizu.Image.Interporation
{
    /// <summary>
    /// Mitchellによる重み付け（ややぼける）
    /// http://entropymine.com/imageworsener/bicubic/
    /// </summary>
    internal class Mitchel : Cubic
    {
        protected override float B { get; } = 1.0f / 3;
        protected override float C { get; } = 1.0f / 3;

        public Mitchel(InterporationOption option) : base (option) { }
    }
}
