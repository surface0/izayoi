﻿using System;

namespace Seizu.Image.Interporation
{
    /// <summary>
    /// Bilinearによる重み付け
    /// </summary>
    internal class Bilinear : DistanceInterporation
    {
        public override float RefRange { get; } = 1.0f;

        public Bilinear(InterporationOption option) : base (option) { }

        public override float CalcWeight(float distance)
        {
            if (distance > RefRange)
            {
                return .0f;
            }

            return 1.0f - Math.Abs(distance);
        }
    }
}
