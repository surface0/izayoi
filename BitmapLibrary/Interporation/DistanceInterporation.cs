﻿/// <summary>
/// 距離による補間のための係数算出する抽象クラス。
/// </summary>
namespace Seizu.Image.Interporation
{
    internal abstract class DistanceInterporation
    {
        /// <summary>
        /// 参照距離。
        /// </summary>
        public virtual float RefRange { get; }

        public DistanceInterporation(InterporationOption option) {}

        /// <summary>
        /// 距離から係数を算出する。
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        public abstract float CalcWeight(float distance);
    }
}
