﻿using System;

namespace Seizu.Image.Interporation
{
    /// <summary>
    /// BSplineによる重み付け
    /// http://entropymine.com/imageworsener/bicubic/
    /// </summary>
    internal class BSpline : Cubic
    {
        protected override float B { get; } = 1.0f;
        protected override float C { get; } = .0f;

        public BSpline(InterporationOption option) : base (option) { }
    }
}
