﻿using System;

namespace Seizu.Image.Interporation
{
    /// <summary>
    /// Cubicリサンプリングのための抽象クラス
    /// http://entropymine.com/imageworsener/bicubic/
    /// </summary>
    abstract internal class Cubic : DistanceInterporation
    {
        public override float RefRange { get; } = 2.0f;

        protected abstract float B { get; }
        protected abstract float C { get; }

        public Cubic(InterporationOption option) : base (option) { }

        public override float CalcWeight(float distance)
        {
            float d = (float)Math.Abs(distance);

            if (d >= 2.0f)
            {
                return .0f;
            }
            else if (d < 1.0f)
            {
                return (
                    (12.0f - 9.0f*B - 6.0f*C)*d*d*d
                    + (-18.0f + 12.0f*B + 6.0f*C)*d*d
                    + (6.0f - 2.0f*B)
                ) / 6.0f;
            }

            return (
                (-B - 6.0f*C)*d*d*d
                + (6.0f*B + 30.0f*C)*d*d
                + (-12.0f*B - 48.0f*C)*d
                + (8.0f*B + 24.0f*C)
            )/ 6.0f;
        }
    }
}
