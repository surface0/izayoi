﻿using System;

namespace Seizu.Image.Interporation
{
    /// <summary>
    /// Catmull-Romによる重み付け（所謂バイキュービック）
    /// http://entropymine.com/imageworsener/bicubic/
    /// </summary>
    internal class CatmullRom : Cubic
    {
        protected override float B { get; } = .0f;
        protected override float C { get; } = .5f;

        public CatmullRom(InterporationOption option) : base (option) { }
    }
}
