﻿using System;

namespace Seizu.Image.Interporation
{
    /// <summary>
    /// Lanczos法による重み付け
    /// </summary>
    internal class Lanczos : DistanceInterporation
    {
        public override float RefRange { get; } = 3.0f;

        public Lanczos(InterporationOption option) : base(option)
        {
            if (option.RefRange != InterporationOption.RefRangeAuto)
            {
                RefRange = option.RefRange;
            }
        }

        protected double sinc(float x)
        {
            return Math.Sin(x * Math.PI) / (x * Math.PI);
        }

        public override float CalcWeight(float distance)
        {
            if (distance > RefRange)
            {
                return .0f;
            }
            else if (distance == .0f)
            {
                return 1.0f;
            }

            return (float)(sinc(distance) * sinc(distance / RefRange));
        }
    }
}
