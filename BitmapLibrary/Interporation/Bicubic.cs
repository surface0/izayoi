﻿using System;

namespace Seizu.Image.Interporation
{
    /// <summary>
    /// バイキュービックによる重み付け（α = 0.75）
    /// http://entropymine.com/imageworsener/bicubic/
    /// </summary>
    internal class Bicubic : Cubic
    {
        protected override float B { get; } = .0f;
        protected override float C { get; } = .75f;

        public Bicubic(InterporationOption option) : base (option) { }
    }
}
