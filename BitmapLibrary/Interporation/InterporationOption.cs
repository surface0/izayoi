﻿using System;
using Seizu.Image.Interporation;

namespace Seizu.Image.Interporation
{
    public class InterporationOption
    {
        public const float RefRangeAuto = -1;

        //public bool GammaCollect = false;
        public float RefRange { get; set; } = RefRangeAuto;

        /// <summary>
        /// デフォルト値の取得
        /// </summary>
        public static InterporationOption Default { get; } = new InterporationOption();
    }
}
