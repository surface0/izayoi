﻿using System;
using System.Numerics;

namespace Seizu.Image.BinaryUtil
{
    internal static class RGBAVectorFactory
    {
        private static Vector4 roundVector = new Vector4(0.5f);

        public static Vector4 createVector4FromInt32(Int32 value)
        {
            return new Vector4(
                value >> 16 & 0xFF,
                value >> 8 & 0xFF,
                value & 0xFF,
                value >> 24 & 0xFF
            );
        }

        public static Int32 convertInt32(Vector4 vector)
        {
            vector += roundVector;
            vector = Vector4.Clamp(vector, new Vector4(0x00), new Vector4(0xFF));
            int a = (int)vector.W;
            int r = (int)vector.X;
            int g = (int)vector.Y;
            int b = (int)vector.Z;
            return a << 24 | r << 16 | g << 8 | b;
        }
    }
}
