﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Seizu.Image.BinaryUtil
{
    internal static class BitmapFactory
    {
        public static Int32[] toInt32Array(Bitmap bmp)
        {
            int length = bmp.Width * bmp.Height;
            var intArray = new Int32[length];
            BitmapData bmpData = bmp.LockBits(new Rectangle(Point.Empty, bmp.Size), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            Marshal.Copy(bmpData.Scan0, intArray, 0, length);
            bmp.UnlockBits(bmpData);

            return intArray;
        }

        public static Bitmap createFromInt32Array(Int32[] intArray, int width, int height)
        {
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            Marshal.Copy(intArray, 0, bmpData.Scan0, bmp.Width * bmp.Height);
            bmp.UnlockBits(bmpData);

            return bmp;
        }
    }
}
