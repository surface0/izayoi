﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Seizu.Image.ColorUtil
{
    public struct Yuv
    {
        public double Y { get; set; }
        public double U { get; set; }
        public double V { get; set; }
        public byte A { get; set; }

        private Yuv(double y, double u, double v, byte a)
        {
            Y = y;
            U = u;
            V = v;
            A = a;
        }

        public static Yuv FromArgb(int argb)
        {
            var c = Color.FromArgb(argb);

            double y = 0.299 * c.R + 0.587 * c.G + 0.114 * c.B;
            double u = -0.14713 * c.R - 0.28886 * c.G + 0.436 * c.B;
            double v = 0.615 * c.R - 0.51499 * c.G - 0.10001 * c.B;
            byte a = c.A;

            return new Yuv(y, u, v, a);
        }

        public int ToArgb()
        {
            byte r = (byte)Math.Min(255, Y + 1.13983 * V);
            byte g = (byte)Math.Min(255, Y - 0.39465 * U - 0.58060 * V);
            byte b = (byte)Math.Min(255, Y + 2.03211 * U);

            return A << 24 | r << 16 | g << 8 | b;
        }
    }
}
