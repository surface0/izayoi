﻿using System;
using System.Drawing;
using Seizu.Image.BinaryUtil;
using System.Threading.Tasks;

namespace Seizu.Image.ColorUtil
{
    internal class Gamma
    {
        protected const double GAMMA = 1.8;

        protected static byte applyGamma(double value,  double gamma) => (byte)(0xFF * Math.Pow((double)value / 0xFF, 1 / gamma));

        public static Bitmap Collect(Bitmap bmp, double gamma = GAMMA)
        {
            int[] srcData = BitmapFactory.toInt32Array(bmp);

            Parallel.For(0, srcData.Length, i =>
            {
                var yuv = Yuv.FromArgb(srcData[i]);
                yuv.Y = applyGamma(yuv.Y, GAMMA);
                srcData[i] = yuv.ToArgb();
            });

            return BitmapFactory.createFromInt32Array(srcData, bmp.Width, bmp.Height);
        }
    }
}
