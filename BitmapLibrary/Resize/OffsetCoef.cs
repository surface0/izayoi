﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seizu.Image.Resize
{
    internal struct OffsetCoef
    {
        public int Offset { get; }
        public float Value { get; }

        public OffsetCoef(int offset, float value)
        {
            Offset = offset;
            Value = value;
        }
    }
}
