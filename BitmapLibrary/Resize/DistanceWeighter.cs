﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Seizu.Image.Interporation;

namespace Seizu.Image.Resize
{
    internal class DistanceWeighter: ResampleWeighter
    {
        protected readonly DistanceInterporation Interporation;

        public DistanceWeighter(DistanceInterporation interporation)
        {
            Interporation = interporation;
        }

        protected static int mirror(int x, int max)
        {
            if (x < 0)
            {
                x = -x;
            }
            else if (x >= max)
            {
                x = max * 2 - x - 1;
            }

            return x;
        }

        public override OffsetCoefList[] MakeWeightListArray(int srclen, int dstlen, int offsetMultiplier = 1)
        {
            float n = Interporation.RefRange;
            float scale = (float)dstlen / srclen;
            float beginOffset = (1.0f / scale - 1.0f) / 2.0f;
            float step = scale > 1.0f ? 1.0f : scale;
            int tap = (int)Math.Ceiling(n / step) * 2;

            var coefLists = new OffsetCoefList[dstlen];

            for (int i = 0; i < dstlen; i++)
            {
                float refOffset = i / scale + beginOffset;
                float distance = -(tap / 2 - 1 + Math.Abs(refOffset) % 1) * step;
                var coefList = new OffsetCoefList(tap);

                for (int j = 0; j <= tap; j++, distance += step)
                {
                    float _distance = Math.Abs(distance);
                    float weight = Interporation.CalcWeight(_distance);
                    int srcOffset = mirror((int)refOffset - tap / 2 + j + 1, srclen);
                    coefList.Add(new OffsetCoef(srcOffset * offsetMultiplier, weight));
                }

                coefLists[i] = coefList;
            }

            return coefLists;
        }
    }
}
