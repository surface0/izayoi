﻿using System;
using System.Collections.Generic;
using Seizu.Image.Interporation;

namespace Seizu.Image.Resize
{
    public enum ResizeInterporation
    {
        NearestNeighbor,
        AreaAverage,
        Bilinear,
        Bicubic,
        Lanczos,
        Spline,
        CatmullRom,
        Mitchel,
        BSpline,
    }

    static class InterporationExtention
    {
        internal static ResampleWeighter GetResampleWeighter(this ResizeInterporation interporation, InterporationOption option)
        {
            switch (interporation)
            {
                case ResizeInterporation.NearestNeighbor:
                    return new DistanceWeighter(new NearestNeighbor(option));

                case ResizeInterporation.AreaAverage:
                    return new AreaAverage();

                case ResizeInterporation.Bilinear:
                    return new DistanceWeighter(new Bilinear(option));

                case ResizeInterporation.Lanczos:
                    return new DistanceWeighter(new Lanczos(option));

                case ResizeInterporation.Spline:
                    return new DistanceWeighter(new Spline(option));

                case ResizeInterporation.CatmullRom:
                    return new DistanceWeighter(new CatmullRom(option));

                case ResizeInterporation.Mitchel:
                    return new DistanceWeighter(new Mitchel(option));

                case ResizeInterporation.BSpline:
                    return new DistanceWeighter(new BSpline(option));

                case ResizeInterporation.Bicubic:
                default:
                    return new DistanceWeighter(new Bicubic(option));
            }
        }
    }
}
