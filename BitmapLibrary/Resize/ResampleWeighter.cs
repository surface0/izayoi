﻿using System;

namespace Seizu.Image.Resize
{
    internal abstract class ResampleWeighter
    {
        abstract public OffsetCoefList[] MakeWeightListArray(int srclen, int dstlen, int offsetMultiplier = 1);
    }
}
