using System;
using System.Threading.Tasks;
using System.Drawing;
using System.Numerics;
using Seizu.Image.BinaryUtil;
using Seizu.Image.Interporation;

namespace Seizu.Image.Resize
{
    internal class ResizeFilter
    {
        protected static int macValue(int[] srcData, OffsetCoefList coefList1, OffsetCoefList coefList2)
        {
            // SIMDを利用するためのVector
            var drgba = new Vector4();

            foreach (var coef2 in coefList2)
            {
                var rgba = new Vector4();
                foreach (var coef1 in coefList1)
                {
                    int srcp = srcData[coef2.Offset + coef1.Offset];
                    var src = RGBAVectorFactory.createVector4FromInt32(srcp);
                    if (src.W > .0f)
                    {
                        rgba += src * coef1.Value;
                    }
                }
                drgba += rgba * coef2.Value;
            }

            float totalWeight = coefList1.TotalWeight * coefList2.TotalWeight;
            
            return RGBAVectorFactory.convertInt32(drgba / totalWeight);
        }

        public static Bitmap Apply(Bitmap bmp, int width, int height, ResizeInterporation interporation, InterporationOption option)
        {
            int[] srcData = BitmapFactory.toInt32Array(bmp);
            int[] dstData = new int[width * height];

            // 縦横個別に重み付け
            var weigher = interporation.GetResampleWeighter(option);
            var weightListsX = weigher.MakeWeightListArray(bmp.Width, width);
            var weightListsY = weigher.MakeWeightListArray(bmp.Height, height, bmp.Width);

            // 回数が多いループを並行にする
            bool swap = weightListsX.Length > weightListsY.Length;
            var weightLists1 = swap ? weightListsX : weightListsY;
            var weightLists2 = swap ? weightListsY : weightListsX;
            int step1 = swap ? 1 : width;
            int step2 = swap ? width : 1;
            
            Parallel.For(0, weightLists1.Length, i1 =>
            {
                for (int i2 = 0; i2 < weightLists2.Length; i2++)
                {
                    int dstIndex = step1 * i1 + step2 * i2;
                    dstData[dstIndex] = macValue(srcData, weightLists1[i1], weightLists2[i2]);
                }
            });

            return BitmapFactory.createFromInt32Array(dstData, width, height);
        }
    }
}
