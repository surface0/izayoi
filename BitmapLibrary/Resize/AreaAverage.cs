﻿using System;
using System.Linq;
using Seizu.Image.Interporation;

namespace Seizu.Image.Resize
{
    /// <summary>
    /// 面積平均法によるリサンプリング
    /// </summary>
    internal class AreaAverage : ResampleWeighter
    {
        public override OffsetCoefList[] MakeWeightListArray(int srcLength, int dstLength, int offsetMultiplier = 1)
        {
            // Euclidean Algorithm
            int gcd(int a, int b) => b == 0 ? a : gcd(b, a % b);
            int lcm = (srcLength * dstLength) / gcd(srcLength, dstLength);

            int srcTap = lcm / srcLength;
            int dstTap = lcm / dstLength;
            int tap = (int)Math.Ceiling((double)dstTap / srcTap);

            //var coefLists = (new OffsetCoefList[dstLength]).Select(x => new OffsetCoefList()).ToArray();
            var coefLists = Array.ConvertAll(new OffsetCoefList[dstLength], x => new OffsetCoefList()); // .NET Standard < 2.0 no supported

            for (int i = 0; i < lcm;)
            {
                int currentSrcp = i / srcTap;
                int currentDstp = i / dstTap;
                int value = 0;

                for (int srcm = i % dstTap; srcm < dstTap; srcm++, value++, i++)
                {
                    if (i / srcTap > currentSrcp || i >= lcm) break;
                }
                
                coefLists[currentDstp].Add(new OffsetCoef(currentSrcp * offsetMultiplier, value)); 
            }

            return coefLists;
        }
    }
}
