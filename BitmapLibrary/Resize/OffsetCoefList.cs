﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Seizu.Image.Resize
{
    internal class OffsetCoefList : Collection<OffsetCoef>
    {
        public OffsetCoefList() : base() { }
        public OffsetCoefList(int capacity) : base(new List<OffsetCoef>(capacity)) { }

        public void Add(int offset, float weight)
        {
            Add(new OffsetCoef(offset, weight));
        }

        public float TotalWeight
        {
            get
            {
                float sum = (from item in this select item.Value).Sum();
                return sum == 0 ? 1 : sum;
            }
        }
    }
}
