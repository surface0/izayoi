using System;
using System.Threading.Tasks;
using System.Drawing;
using System.Numerics;
using Seizu.Image.BinaryUtil;

namespace Seizu.Image.Resize
{
    internal class RotateFilter
    {
        protected static int macValue(int[] srcData, OffsetCoefList coefList1, OffsetCoefList coefList2)
        {
            // SIMDを利用するためのVector
            var drgba = new Vector4();

            foreach (var coef2 in coefList2)
            {
                var rgba = new Vector4();
                foreach (var coef1 in coefList1)
                {
                    int srcp = srcData[coef2.Offset + coef1.Offset];
                    var src = RGBAVectorFactory.createVector4FromInt32(srcp);
                    if (src.W > .0f)
                    {
                        rgba += src * coef1.Value;
                    }
                }
                drgba += rgba * coef2.Value;
            }

            float totalWeight = coefList1.TotalWeight * coefList2.TotalWeight;
            
            return RGBAVectorFactory.convertInt32(drgba / totalWeight);
        }

        protected static double sinc(double x)
        {
            return Math.Sin(x * Math.PI) / (x * Math.PI);
        }

        public static Bitmap Apply(Bitmap bmp, double angle, ResizeInterporation interporation)
        {
            double sm = Math.Sin(-angle);
            double cm = Math.Cos(-angle);
            double sp = Math.Sin(angle);
            double cp = Math.Cos(angle);

            int dstW = (int)(Math.Truncate(Math.Abs(bmp.Width * cp) + Math.Abs(bmp.Height * sp) + 0.5));
            int dstH = (int)(Math.Truncate(Math.Abs(bmp.Width * sp) + Math.Abs(bmp.Height * cp) + 0.5));

            int[] srcData = BitmapFactory.toInt32Array(bmp);
            int[] dstData = new int[dstW * dstH];

            double cxd = dstW / 2.0d;
            double cyd = dstH / 2.0d;
            double cxs = bmp.Width / 2.0d;
            double cys = bmp.Height / 2.0d;
            int bw = bmp.Width;
            int bh = bmp.Height;

            Parallel.For(0, dstW, x2 =>
            {
                for (int y2 = 0; y2 < dstH; y2++)
                {
                    // 元画像に於ける画素の対応する場所
                    double x0 = ((x2 - cxd) * cp - (y2 - cyd) * sp) + cxs;
                    double y0 = ((x2 - cxd) * sp + (y2 - cyd) * cp) + cys;
                    int x1 = (int)Math.Floor(x0);
                    int y1 = (int)Math.Floor(y0);

                    if ((x1 >= 0 && x1 < bw) && (y1 >= 0 && y1 < bh))
                    {
                        float totalWeight = 0;
                        var c = RGBAVectorFactory.createVector4FromInt32(0);

                        for (double ix = -3.0d; ix <= 3.0d; ix++)
                        {
                            for (double iy = -3.0d; iy <= 3.0d; iy++)
                            {
                                int xx1 = Math.Max(0, Math.Min(bw - 1, (int)Math.Floor(x1 + ix)));
                                int yy1 = Math.Max(0, Math.Min(bh - 1, (int)Math.Floor(y1 + iy)));
                                double dx = Math.Abs(ix - x0 % 1);
                                double dy = Math.Abs(iy - y0 % 1);
                                double wx = dx == .0d ? 1.0d : (sinc(dx) * sinc(dx / 6.0d));
                                double wy = dy == .0d ? 1.0d : (sinc(dy) * sinc(dy / 6.0d));
                                float weight = (float)(wx * wy);
                                var src = srcData[xx1 + yy1 * bw];
                                c += RGBAVectorFactory.createVector4FromInt32(src) * weight;
                                totalWeight += weight;
                            }
                        }

                        dstData[x2 + y2 * dstW] = RGBAVectorFactory.convertInt32(c / totalWeight);
                    }
                }
            });

            return BitmapFactory.createFromInt32Array(dstData, dstW, dstH);
        }
    }
}
