﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using Mono.Options;
using Seizu.Image;
using Seizu.Image.Resize;
using System.Drawing;


namespace Izayoi
{
    class Program
    {
        static void Main(string[] args)
        {
            bool showHelp = false;
            int resizeMax = 640;
            string inputFile = "";
            string outputFile = "";

            var p = new OptionSet()
            {
                {"h|help", "show help.", v => showHelp = v != null},
                {"s|size=", "max size", (int v) => resizeMax = v}
            };

            Stack<string> extra;

            try
            {
                extra = new Stack<string>(p.Parse(args));
            }
            //パースに失敗した場合OptionExceptionを発生させる
            catch (Exception e)
            {
                Console.WriteLine("Izayoi:");
                Console.WriteLine(e.Message);
                Console.WriteLine("Try `CommandLineOption --help' for more information.");
                return;
            }

            if (showHelp)
            {
                ShowHelp(p);
                return;
            }

            extra.ToList().ForEach(t => Debug.WriteLine(t));
            outputFile = extra.Pop();
            inputFile = extra.Pop();

            if (!File.Exists(inputFile))
            {
                Console.Error.WriteLine("input file not found.");
                return;
            }

            rotate(inputFile, outputFile);

            //resize(inputFile, outputFile, resizeMax);

            Console.WriteLine("Finish.");

#if DEBUG
            Console.ReadLine();
#endif
        }

        static void ShowHelp(OptionSet p)
        {
            Console.Error.WriteLine("Usage:Izayoi [OPTIONS]");
            Console.Error.WriteLine();
            p.WriteOptionDescriptions(Console.Error);
        }

        static void rotate(string src, string dst)
        {
            var sbmp = new Bitmap(src);

            var dbmp = BitmapUtil.Rotate(sbmp, 20.5* Math.PI / 180);

            dbmp.Save(dst);
        }

        static void resize(string src, string dst, int resizeMax)
        {
            var sbmp = new Bitmap(src);
            int dw, dh;

            if (sbmp.Width >= sbmp.Height)
            {
                dw = resizeMax;
                dh = (int)((double)resizeMax / sbmp.Width * sbmp.Height);
            }
            else
            {
                dw = (int)((double)resizeMax / sbmp.Height * sbmp.Width);
                dh = resizeMax;
            }

            var dbmp = BitmapUtil.Resize(sbmp, dw, dh, ResizeInterporation.CatmullRom);

            /*
            Encoder myEncoder = Encoder.Quality;

            var myEncoderParameter = new EncoderParameter(myEncoder, 100L);
            var myEncoderParameters = new EncoderParameters(1);
            myEncoderParameters.Param[0] = myEncoderParameter;

            var jpgEncoder = ImageCodecInfo.GetImageDecoders().Where(x => x.FormatID == ImageFormat.Jpeg.Guid).First();

            dbmp.Save(dst, jpgEncoder, myEncoderParameters);
            */
            dbmp.Save(dst);
        }
    }
}
