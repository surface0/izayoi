﻿using System;
using System.Diagnostics;

namespace BitmapLibraryTest
{
    public static class StopwatchExtension
    {
        public static void DumpReset(this Stopwatch sw, string msg)
        {
            sw.Stop();
            Console.WriteLine(String.Format("[{0}] Elapsed: {1:f2}ms", msg, sw.Elapsed.TotalMilliseconds));
            sw.Reset();
        }
    }
}
