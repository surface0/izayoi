using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.IO;
using System.Diagnostics;
using Seizu.Image;
using Seizu.Image.Resize;
using Seizu.Image.Interporation;

namespace BitmapLibraryTest
{
    [TestClass]
    public class UnitTest1
    {
        static private string srcFile = @"sample.png";
        static private string dstDir = @"dst";
        static private Bitmap bmp;

        [TestMethod]
        public void TestMethod1()
        {
            using (bmp = new Bitmap(srcFile))
            {
                if (!Directory.Exists(dstDir))
                {
                    Directory.CreateDirectory(dstDir);
                }

                Directory.SetCurrentDirectory(dstDir);

                var sw = new Stopwatch();

                sw.Start();

                ResizeTest(0.7f);

                sw.DumpReset("Total");

                Console.ReadLine();
            }
        }

        static void ResizeTest(float resizeScale)
        {
            Bitmap dst;
            var options = new InterporationOption();
            var sw = new Stopwatch();

            int dw = (int)(bmp.Width * resizeScale + 0.5);
            int dh = (int)(bmp.Height * resizeScale + 0.5);

            sw.Start();
            dst = BitmapUtil.Resize(bmp, dw, dh, ResizeInterporation.NearestNeighbor, options);
            sw.DumpReset("NearestNeighbor");
            dst.Save(Path.ChangeExtension(srcFile, @".NearestNeighbor.png"));

            sw.Start();
            dst = BitmapUtil.Resize(bmp, dw, dh, ResizeInterporation.AreaAverage, options);
            sw.DumpReset("AreaAverage.png");
            dst.Save(Path.ChangeExtension(srcFile, @".AreaAverage.png"));

            sw.Start();
            dst = BitmapUtil.Resize(bmp, dw, dh, ResizeInterporation.Bilinear, options);
            sw.DumpReset("Bilinear");
            dst.Save(Path.ChangeExtension(srcFile, @".Bilinear.png"));

            sw.Start();
            dst = BitmapUtil.Resize(bmp, dw, dh, ResizeInterporation.Bicubic, options);
            sw.DumpReset("Bicubic");
            dst.Save(Path.ChangeExtension(srcFile, @".Bicubic.png"));

            sw.Start();
            options.RefRange = 3;
            dst = BitmapUtil.Resize(bmp, dw, dh, ResizeInterporation.Spline, options);
            sw.DumpReset("Spline16");
            dst.Save(Path.ChangeExtension(srcFile, @".Spline16.png"));

            sw.Start();
            options.RefRange = 4;
            dst = BitmapUtil.Resize(bmp, dw, dh, ResizeInterporation.Spline, options);
            sw.DumpReset("Spline36");
            dst.Save(Path.ChangeExtension(srcFile, @".Spline36.png"));

            sw.Start();
            options.RefRange = 5;
            dst = BitmapUtil.Resize(bmp, dw, dh, ResizeInterporation.Spline, options);
            sw.DumpReset("Spline64");
            dst.Save(Path.ChangeExtension(srcFile, @".Spline64.png"));

            sw.Start();
            options.RefRange = 3;
            dst = BitmapUtil.Resize(bmp, dw, dh, ResizeInterporation.Lanczos, options);
            sw.DumpReset("Lanczos2");
            dst.Save(Path.ChangeExtension(srcFile, @".Lanczos2.png"));

            sw.Start();
            options.RefRange = 4;
            dst = BitmapUtil.Resize(bmp, dw, dh, ResizeInterporation.Lanczos, options);
            sw.DumpReset("Lanczos3");
            dst.Save(Path.ChangeExtension(srcFile, @".Lanczos3.png"));

            sw.Start();
            options.RefRange = 5;
            dst = BitmapUtil.Resize(bmp, dw, dh, ResizeInterporation.Lanczos, options);
            sw.DumpReset("Lanczos4");
            dst.Save(Path.ChangeExtension(srcFile, @".Lanczos4.png"));
        }
    }
}
